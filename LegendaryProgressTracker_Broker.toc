## Interface: 70300
## Title: LegendaryProgressTracker_Broker
## Version: 1.0.0
## Notes: DataBroker plugin for the Legendary Progress Tracker
## RequiredDeps: LegendaryProgressTracker

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

LegendaryProgressTracker_Broker.lua
