local LPT = LibStub("AceAddon-3.0"):GetAddon("LPT")
local LDB = LibStub:GetLibrary("LibDataBroker-1.1")

if not LPT then return end

local DAY = DAY

local tonumber = tonumber
local string_format = string.format
local time = time

LPT.LDB = LDB:NewDataObject("LegendaryProgressTracker_Broker", {
	type = "data source",
	icon = "Interface\\Icons\\INV_Legendary_Sword",
	text = "LPT"
})

-- taken from LPT:PrintEvents()
local function FillTooltip(tooltip)
	if historical.numLegs == 0 then 
		tooltip:AddLine("Reporting events since install")
	else
		tooltip:AddLine("Reporting events since last legendary")
	end
	tooltip:AddLine("Emissary Chests: " .. lptEvents.emissaryChest, 1, 1, 1)
	tooltip:AddLine("Weekly Chest: " .. lptEvents.weeklyChest, 1, 1, 1)
	tooltip:AddLine("Paragon Rep Chests: " .. lptEvents.paragonChest, 1, 1, 1)
	tooltip:AddLine("Argus Chests (Lesser; Greater): " .. lptEvents.argusLesserChest .. "; " .. lptEvents.argusGreaterChest, 1, 1, 1)
	tooltip:AddLine("Argus Rares: " .. lptEvents.argusRare, 1, 1, 1)
	tooltip:AddLine("Argus Invasions (Lesser; Greater): " .. lptEvents.lesserInvasion .. "; " .. lptEvents.greaterInvasion, 1, 1, 1)
	--tooltip:AddLine("Normal Mode Dungeons: " .. lptEvents.normalDungeon, 1, 1, 1)
	tooltip:AddLine("Heroic Dungeons: " .. lptEvents.heroicDungeon, 1, 1, 1)
	tooltip:AddLine("Broken Shore Rares: " .. lptEvents.islandRare, 1, 1, 1)
	tooltip:AddLine("Mythic 0 Dungeon Bosses: " .. lptEvents.mythicDungeon, 1, 1, 1)
	tooltip:AddLine("Mythic + Dungeons: " ..lptEvents.mPlusDungeon, 1, 1, 1)
	tooltip:AddLine("Raid bosses (LFR; Normal; Heroic; Mythic; Non Current Raid): " .. lptEvents.lfr .. "; " .. 
		lptEvents.normalRaid .. "; " .. lptEvents.heroicRaid .. "; " .. lptEvents.mythicRaid.. "; " .. lptEvents.oldRaid, 1, 1, 1)
	tooltip:AddLine("World Bosses: " .. lptEvents.worldBoss, 1, 1, 1)
	tooltip:AddLine("PvP events: " .. lptEvents.pvp, 1, 1, 1)
	tooltip:AddLine("War Supplies Caches: " .. lptEvents.warSupplies, 1, 1, 1)
	tooltip:AddLine("Blingtron 6000: " .. lptEvents.blingtron, 1, 1, 1)
	if historical.numLegs == 0 then 
		tooltip:AddLine("Time since install of addon " .. tonumber(string_format("%.1f", (time() - historical.installedDate) / DAY)) .. " days", 1, 1, 1)
	else
		tooltip:AddLine("Time since last legendary " .. tonumber(string_format("%.1f", (time() - lastLegInfo.date) / DAY)) .. " days", 1, 1, 1)
	end
	tooltip:AddLine(LPT:GetBLP() .. "% towards bad luck protection cap")
end

function LPT.LDB:OnTooltipShow()
	FillTooltip(self)
end
